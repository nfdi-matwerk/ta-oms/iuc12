---
title: Home
layout: home
nav_order: 1
permalink: /
---
![WG logo](/assets/img/matwerk.png "WG logo")

# NFDI-MatWerk LOD Working Group

Linked Open Data defines a vision of globaly accessible and linked data on the internet based on the RDF standards of the semantic web. Tim Berners-Lee presented a set of four basic rules for publishing, connecting and consuming the structured data on the Web (LOD rules).

The goal of this working group is to address the problems that participant projects face when working with or towards LOD. The information collected in LOD working group will be integrated into <a href="https://demo.fiz-karlsruhe.de/matwerk/" target="_blank">the MSE knowledge graph</a>. 

## Why is this working group important? 

Joining the Linked Open Data (LOD) working group is a great way for materials scientists and engineers to improve how they use and share research data. When data is published as LOD, it becomes easily accessible and reusable for researchers and developers. One significant advantage is the ability to link data, allowing users to enhance it with external resources. This can include adding missing data or introducing new information. It's a collaborative way to improve and model the data. The LOD working group provides a space for materials scientists to overcome these challenges together and be part of a community focused on making research data more accessible and impactful.

## How to contribute?

We now have a new mailing list: **nfdi-matwerk_lod@lists.nfdi.de**

To be included in the mailing list, you have to send an EMail to <a href="mailto:nfdi-matwerk@iwm.fraunhofer.de">Maria Baffiera</a> mentioning that you are going to join the LOD-WG mailing list.

To join this working group, you can also fill out this <a href="https://docs.google.com/forms/d/e/1FAIpQLSeGnQG0IKksoTpJdYc_PAUUgeXGS3aaO9feNUD5A_UETo_WKQ/viewform" target="_blank">survey</a> or send an <a href="mailto:ebrahim.norouzi@fiz-Karlsruhe.de">email</a> to us.

## What are the requirements?

We strongly suggest you these learning materials:

* <a href="https://open.hpi.de/courses/knowledgegraphs2023" target="_blank">Knowledge Graphs - Foundations and Applications</a>
* <a href="https://youtube.com/playlist?list=PLNXdQl4kBgzuT1gutt-5EnFc1OGsXnjCC&si=xOj5lH1tyNmTrfkR" target="_blank">ISE 2021 YouTube Lecture (lectures 6-9) </a>
* <a href="https://www.w3.org/TR/ldp-bp/" target="_blank">Linked Data Platform Best Practices and Guidelines </a>

## What are the useful tools?

* We use Trello for agile management (<a href="https://trello.com/invite/b/4bIWhF8I/ATTIeea22460ba608ff5a4a815fdaaa503396CFB2CC1/linked-open-data-wg" target="_blank">click here for joining!</a>).
* Here is <a href="https://drive.google.com/drive/folders/1z5_jz_7opU4vHO2wWHrRGzfTq6BBhR6N?usp=sharing" target="_blank">the link</a> for the Google drive for sharing the meeting notes and presentation files.
* <a href="https://miroboard.com" target="_blank">Miro board</a>
* <a href="https://git.rwth-aachen.de/nfdi-matwerk/ta-oms/iuc12" target="_blank">Gitlab Page</a>


