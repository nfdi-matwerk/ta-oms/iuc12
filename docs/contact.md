---
layout: default
title: Contact
nav_order: 3
---

Contact:

* <a href="https://demo.fiz-karlsruhe.de/matwerk/E15879.html" target="_blank">Ebrahim Norouzi</a>, <a href="mailto:ebrahim.norouzi@fiz-Karlsruhe.de">Send an email</a>
    Scientific Researcher, Information Service Engineering,
    <a href="https://demo.fiz-karlsruhe.de/matwerk/E1016.html" target="_blank">FIZ Karlsruhe – Leibniz Institute for Information Infrastructure</a>    

* <a href="https://demo.fiz-karlsruhe.de/matwerk/E1247883.html" target="_blank">Matwerk- Ontologies for Materials Science (TA-OMS)</a>, <a href="mailto:MatWerkOMS@fz-juelich.de">Send an email</a>




